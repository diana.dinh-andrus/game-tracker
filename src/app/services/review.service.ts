import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Review } from 'src/app/models/review';
import { map } from 'rxjs/operators';
// import { Review } from '../models/review';

const BASE_API = environment.url;

@Injectable({
  providedIn: 'root',
})
export class ReviewService {
  constructor(private httpClient: HttpClient) {}

  getReviewsByGameId(gameId: string) {
    return this.httpClient.get(
      environment.url + 'games/' + gameId + '/reviews/'
    );
  }

  getReviewsByUser(userId: string) {
    return this.httpClient.get(environment.url + `user/${userId}/reviews`)
  }

  submitReview(
    userId: string,
    rating: string,
    review: string,
    gameId: string
  ): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    const body = JSON.stringify({
      reviewRating: rating,
      reviewBody: review,
      user: { userId: userId },
      game: { id: gameId },
    });
    return this.httpClient.post(BASE_API + 'user/reviews', body, httpOptions);
  }

  submitComments(comments: string, userId:string, gameId: string):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    const body = JSON.stringify({'textbody':comments, user:{'userId':userId}, game:{'id': gameId}});
    return this.httpClient.post(BASE_API + 'user/comment', body, httpOptions);
  }

  getCommentsByGameAndUserIds(userId:string, gameId:string):Observable<Comment[]>{
    const HttpHeaders = ({ 'Accept': 'application/json' })
    const params = new HttpParams()
    .set('game', gameId)
    .set('user', userId);
    // .append('userId', userId)
    // .append('id', gameId);

    // const body = JSON.stringify({user:{'userId':userId}, game:{'id': gameId}});
    
    return this.httpClient.get<Comment[]>(BASE_API + 'games/comment', {headers: HttpHeaders, params: params, responseType: 'json'});
  }
  
}
