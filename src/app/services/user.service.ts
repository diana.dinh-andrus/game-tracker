import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Game } from '../models/game';
import { User } from '../models/user';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  addFavoriteGame(gameId: string, userId: string) {
    const params = new HttpParams()
      .append('User', userId)
      .append('Game', gameId);

    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.httpClient.post(env.url + 'user/favorites', httpOptions, {
      params: params,
    });
  }

  deleteGameFromFavorite(favId: string) {
    return this.httpClient
      .delete(env.url + `user/${favId}/favorites`)
      .toPromise();
  }
}
