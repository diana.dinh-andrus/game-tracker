import { _fixedSizeVirtualScrollStrategyFactory } from '@angular/cdk/scrolling';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Game } from '../models/game';
import { environment as env } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private httpClient: HttpClient) {}

  getAllGames(): Observable<any> {
    return this.httpClient.get(env.url + 'games');
  }

  getGameById(gameId: string) {
    return this.httpClient.get(env.url + 'games/' + gameId);
  }

  getGameByUserId(userId: string) {
    const params = new HttpParams().append('user_id', userId);

    return this.httpClient.get(env.url + 'user/games', { params: params });
  }
}
