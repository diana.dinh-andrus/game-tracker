import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { GenreService } from 'src/app/services/genre.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn = false;
  user!: User;
  genres!: string[];
  platforms: string[]  = ["xbox", "pc"];
  email: string = '';
  password: string = '';

  constructor(
    private loginService: LoginService, 
    private _router: Router,
    private genreService: GenreService
    ) { }

  ngOnInit(): void {
    if (this.isLoggedIn) {
      this.user = this.loginService.getUser();
    }
    this.genreService.getGenreList().toPromise()
    .then(res => this.genres = res as string[]);
  }

  loggedIn() {
    return this.loginService.getToken();
  }


  logout(): void {
    this.loginService.logout();
    this._router.navigate(["/"]);
  }

  dropDownClick(path: string, item: string) {
    this._router.navigate([path, item]);
  }

}
