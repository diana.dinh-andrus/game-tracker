export interface Game {
  id: string;
  title: string;
  availableDate: string;
  availableEnd: string;
  imageUrl: string;
  genre: string;
  xbox: boolean;
  pc: boolean;
}
