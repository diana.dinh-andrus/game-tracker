import { Game } from "./game";
import { User } from "./user";

export interface Comment {
    commentId: string,
    textbody: string,
    user: User,
    game: Game
}
