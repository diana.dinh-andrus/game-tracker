export interface User {
  userId: string;
  fname: string;
  lname: string;
  email: string;
  role: string;
  userName: string;
}
