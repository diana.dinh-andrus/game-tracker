import { Game } from './game';
import { User } from './user';

export interface Review {
  id: number;
  reviewRating: string;
  reviewBody: string;
  user: User;
  game: Game;
}
