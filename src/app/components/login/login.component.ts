import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  hide = true;
  loginInvalid = false;

  private _unsubscribeAll: Subject<any>;


  constructor(private loginService: LoginService, private _router: Router, private fb: FormBuilder) {
    this.loginForm = this.fb.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
    this._unsubscribeAll = new Subject<any>();
   }

  ngOnInit(): void {
  }

  login(){
    if (this.loginForm.valid) {
      this.loginInvalid = true;
      const email = this.loginForm.get('email')?.value;
      const password = this.loginForm.get('password')?.value;

      this.loginService.login(email, password)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
        if (data) {
          this.loginInvalid = false;
          this.loginService.saveToken("myToken");
          this.loginService.saveUser(data);
          this._router.navigate(["/"]);
        } else {
          this.loginInvalid = true;
        }
      })
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
