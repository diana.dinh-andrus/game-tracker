import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Game } from 'src/app/models/game';
import { Review } from 'src/app/models/review';
import { User } from 'src/app/models/user';
import { GameService } from 'src/app/services/game.service';
import { LoginService } from 'src/app/services/login.service';
import { ProfileService } from 'src/app/services/profile.service';
import { ReviewService } from 'src/app/services/review.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  user!: User;
  city: string = '';
  country: string = '';
  favoriteGames!: any;
  reviews!: Review[];

  private _unsubscribeAll: Subject<any>;

  constructor(
    private loginService: LoginService,
    private profileService: ProfileService,
    private userService: UserService,
    private reviewService: ReviewService,
    private _router: Router,
    private gameService: GameService) {
    this._unsubscribeAll = new Subject<any>();
  }

  async ngOnInit(): Promise<void> {
    this.user = await this.loginService.getUser();
    if (this.user) {
      this.gameService.getGameByUserId(this.user.userId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(data => {
          this.favoriteGames = data;
        })
        this.reviewService.getReviewsByUser(this.user.userId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(data => {
          this.reviews = data as Review[];
        })
    }
    this.profileService.getLocation()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: any) => {
        this.city = data.city;
        this.country = data.countryCode;
      })
  }

  deleteFromFavorite(favId: string) {
    this.userService.deleteGameFromFavorite(favId)
      .catch(e => console.log(e));

    this.favoriteGames = this.favoriteGames.filter((data: any) => data.favId !== favId)
  }

  goToMovieDetails(gameid: string) {
    this._router.navigate(['games', gameid])
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
