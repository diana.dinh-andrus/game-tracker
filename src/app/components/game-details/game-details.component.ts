import { Component, OnDestroy, OnInit } from '@angular/core';
import { Game } from 'src/app/models/game';
import { User } from 'src/app/models/user';
import { GameDetailsService } from 'src/app/services/game-details.service';
import { LoginService } from 'src/app/services/login.service';
import { GameService } from 'src/app/services/game.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Review } from 'src/app/models/review';
import { ReviewService } from 'src/app/services/review.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css'],
})
export class GameDetailsComponent implements OnInit, OnDestroy {
  game!: Game;
  user!: User;
  reviews!: Review[];
  gameId: string = '';
  rating: string = '';
  reviewForm: FormGroup;
  commentsForm!: FormGroup;
  reviewInvalid: boolean = false;
  selected: string = '';
  headingContents = 'Submit Review Here';
  commentHeadingContents = 'Submit Comments Here';
  userFavoriteGames!: Game[];
  addedToUserFavorite = false;

  comment!: Comment;
  comments!: any;

  private _unsubscribeAll: Subject<any>;

  constructor(
    private gameDetailsService: GameDetailsService,
    private loginService: LoginService,
    private _route: ActivatedRoute,
    private gameService: GameService,
    private reviewService: ReviewService,
    private _userservice: UserService,
    private _router: Router,
    private fb: FormBuilder
  ) {
    this.reviewForm = this.fb.group({
      rating: ['', Validators.required],
      review: ['', Validators.required],
    });
    this.commentsForm = this.fb.group({
      comments: ['', Validators.required],
    });
    this._unsubscribeAll = new Subject<any>();
  }

  writeReview() {
    this.gameDetailsService.writeReview(this.game, this.user);
  }

  submit() {
    if (this.reviewForm.valid) {
      // const user: User = this.loginService.getUser();
      const rating = this.reviewForm.get('rating')?.value;
      const review = this.reviewForm.get('review')?.value;
      this.reviewService
        .submitReview(this.user.userId, rating, review, this.game.id)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe();
      this.reviewForm.reset();
      this.reviews.push({
        id: Number(this.user.userId),
        reviewRating: rating,
        reviewBody: review,
        user: this.user,
        game: this.game,
      });
      this.headingContents = 'Review Submitted';
    }
  }

  submitComments() {
    if (this.commentsForm.valid) {
      const comments = this.commentsForm.get('comments')?.value;
      this.reviewService
        .submitComments(comments, this.user.userId, this.game.id)
        .subscribe();
      this.commentsForm.reset();
      // this.comments.push();

      this.commentHeadingContents = 'Comments submitted';
    }
  }

  async ngOnInit(): Promise<void> {
    this._route.paramMap.subscribe(async (params) => {
      const gameId = params.get('id');
      if (gameId) {
        this.reviewService.getReviewsByGameId(gameId).subscribe((data) => {
          this.reviews = data as Review[];
        });
        this.gameService
          .getGameById(gameId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((data) => {
            if (data) {
              this.game = data as Game;
            }
          });
      }
    });
    this.user = await this.loginService.getUser();
    if (this.user) {
      await this.gameService
        .getGameByUserId(this.user.userId)
        .toPromise()
        .then(async (data: any) => {
          const games = await data.map((fav: any) => fav.game);
          this.userFavoriteGames = games;
          this.addedToUserFavorite = this.checkFav();
        });
      await this.reviewService
        .getCommentsByGameAndUserIds(this.user.userId, this.game.id)
        .toPromise()
        .then(async (data: any) => {
          this.comments = data;
        });
    }
  }

  addToFavorite(event: Event, game: Game) {
    if (this.user && !this.checkFav()) {
      const button = event.target as HTMLButtonElement;
      button.disabled = true;
      button.style.color = 'rgb(120, 134, 172)';
      this._userservice
        .addFavoriteGame(game.id, this.user.userId)
        .toPromise()
        .catch((e) => console.log(e));
    } else {
      this._router.navigate(['login']);
    }
  }

  checkFav() {
    const found = this.userFavoriteGames?.find(g => g.id === this.game.id)
    if (found) {
      return true;
    }
    return false;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
