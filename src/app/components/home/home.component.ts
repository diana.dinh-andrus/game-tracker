import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Game } from 'src/app/models/game';
import { GameService } from 'src/app/services/game.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  trendingGames!: Game[];
  recentlyAddedGames!: Game[];
  featuredGames!: Game[];

  private _unsubscribeAll: Subject<any>;

  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    center: true,
    dots: true,
    animateOut: 'fadeOut',
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      }
    }
  }

  constructor(private gameService: GameService, private readonly http: HttpClient) {
    this._unsubscribeAll = new Subject<any>();
   }

  ngOnInit(): void {
    this.gameService.getAllGames()
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe(data => {
      this.trendingGames = data.slice(-4);
      this.recentlyAddedGames = data.slice(-4);
      this.featuredGames = data.slice(-3);
    })
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
