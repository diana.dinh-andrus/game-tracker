import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Game } from 'src/app/models/game';
import { GameService } from 'src/app/services/game.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { map, takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})

export class GamesComponent implements OnInit, OnDestroy {
  games!: Game[];

  loading = true;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  obs!: Observable<any>;
  dataSource!: MatTableDataSource<Game>;
  title: string = 'Gamepass Now';

  user!: User;
  userFavoriteGames: Game[];

  private _unsubscribeAll: Subject<any>;


  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private _gameService: GameService,
    private _loginService: LoginService,
    private _userservice: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.userFavoriteGames = [];
    this._unsubscribeAll = new Subject<any>();
  }


  async ngOnInit(): Promise<void> {
    this._gameService.getAllGames()
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe(data => {
      this._route.paramMap
      .subscribe(async params => {
        const filter = params.get('id');
        if (filter) {
          this.title = filter;
          if (filter === 'xbox' || filter === 'pc') {
            if (filter === 'xbox') {
              this.games = data.filter((game: Game) => game.xbox === true);
            } else {
              this.games = data.filter((game: Game) => game.pc === true);
            }
          } else {
            this.games = data.filter((game: Game) => game.genre.toLowerCase().includes(filter.toLowerCase()));
          }
        } else {
          this.games = data;
        }
        this.dataSource = new MatTableDataSource<Game>(this.games);
        this.changeDetectorRef.detectChanges();
        this.dataSource.paginator = this.paginator;
        this.obs = this.dataSource.connect();
        this.loading = false;
      })
    })

    this.user = await this._loginService.getUser();
    if (this.user) {
     await this._gameService.getGameByUserId(this.user.userId).toPromise()
     .then(async (data: any)=> {
       const userGames = await data.map((fav: any) => fav.game)
       this.userFavoriteGames = userGames;
     })
    }
  }

  getGameDetails(id: number) {
    this._router.navigate(['games', id]);
  }

  ngOnDestroy(): void {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  applyFilter(event: Event): void {
    // get the input value
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addToFavorite(game: Game) {
    const alreadyInFav = this.checkFav(this.userFavoriteGames, game);
    if (this.user && !alreadyInFav) {
      this._userservice.addFavoriteGame(game.id, this.user.userId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
        if (data) {
          this._router.navigate(["profile"])
        }
      })
    }
  }

  checkFav(gameArr: Game[], game: Game){
    const found = gameArr.find(g => g.id === game.id)
    if (found) {
      return true
    }
    return false
  }

}
